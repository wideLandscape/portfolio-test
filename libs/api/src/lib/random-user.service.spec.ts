import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { inject, TestBed } from '@angular/core/testing';
import { RandomUserApiResult } from '@portfolio-test/data-models';
import { RandomUserService } from './random-user.service';
import { API_URL } from './_configs';

describe('RandomUserService', () => {
  let service: RandomUserService;
  let httpTestingController: HttpTestingController;
  let randomUser: RandomUserApiResult;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    randomUser = {
      results: [
        {
          name: { title: 'Monsieur', first: 'Jacques', last: 'Vidal' },
          location: {
            street: { number: 6000, name: "Rue de L'Abbé-De-L'Épée" },
          },
          email: 'jacques.vidal@example.com',
          login: {
            username: 'redsnake528',
            password: 'toby',
          },
          dob: { date: '1966-05-01T02:08:14.446Z', age: 55 },
          phone: '079 705 64 61',
          cell: '077 198 82 88',
          picture: {
            large: 'https://randomuser.me/api/portraits/men/4.jpg',
            medium: 'https://randomuser.me/api/portraits/med/men/4.jpg',
            thumbnail: 'https://randomuser.me/api/portraits/thumb/men/4.jpg',
          },
        },
      ],
    };
  });

  beforeEach(inject(
    [RandomUserService],
    (randomUserService: RandomUserService) => {
      service = randomUserService;
    }
  ));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return data', () => {
    let result: RandomUserApiResult;
    service.request().subscribe({
      next: (t) => {
        result = t;
        expect(result).toEqual(randomUser);
      },
    });
    const req = httpTestingController.expectOne({
      url: API_URL,
      method: 'GET',
    });

    req.flush([randomUser]);
  });

  it('should throw error', () => {
    let error: Error;
    service.request().subscribe({
      error: (e) => {
        error = e;
        expect(
          error.message.indexOf(
            'Http failure response for https://randomuser.me/api: 404 Network error'
          ) >= 0
        ).toBeTruthy();
        expect(error.name).toEqual('HttpErrorResponse');
      },
    });

    const req = httpTestingController.expectOne({
      url: API_URL,
      method: 'GET',
    });
    req.flush('Something went wrong', {
      status: 404,
      statusText: 'Network error',
    });
  });
});
