import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
export * from './random-user.service';

@NgModule({
  imports: [CommonModule, HttpClientModule],
})
export class ApiModule {}
