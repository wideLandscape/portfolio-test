import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { API_URL } from './_configs';
import { RandomUserApiResult } from '@portfolio-test/data-models';
@Injectable({
  providedIn: 'root',
})
export class RandomUserService {
  constructor(private http: HttpClient) {}

  request() {
    return this.http.get<RandomUserApiResult>(API_URL);
  }
}
