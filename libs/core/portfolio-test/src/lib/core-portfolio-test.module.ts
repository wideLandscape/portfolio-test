import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiModule } from '@portfolio-test/api';
@NgModule({
  imports: [CommonModule, ApiModule],
})
export class CorePortfolioTestModule {}
