import { Injectable } from '@angular/core';
import { RandomUserService } from '@portfolio-test/api';
import { RandomUser } from '@portfolio-test/data-models';
import { Observable, Subject } from 'rxjs';
import {
  catchError,
  filter,
  first,
  map,
  pluck,
  retry,
  timeout,
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SandboxService {
  randomUser$: Observable<RandomUser> = this.service.request().pipe(
    retry(0),
    timeout(15000),
    first(),
    catchError((e) => this.handleError(e)),
    pluck('results'),
    filter((users) => users.length > 0),
    map((users) => users[0])
  );

  error$: Subject<Error> = new Subject<Error>();

  constructor(private service: RandomUserService) {}

  private handleError(error: Error) {
    console.error(error.name, error.message);
    this.error$.next(error);
    return [];
  }
}
