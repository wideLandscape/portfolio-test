import { TestBed } from '@angular/core/testing';
import { ApiModule } from '@portfolio-test/api';
import { SandboxService } from './sandbox.service';

describe('SandboxService', () => {
  let service: SandboxService;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [ApiModule] });
    service = TestBed.inject(SandboxService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
