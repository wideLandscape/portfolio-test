import { IconType, IconTypeXAxisMap } from '@portfolio-test/data-models';

const iconWidth = 68;

export const iconTypesXAxisMap: IconTypeXAxisMap = {
  name: 0,
  email: iconWidth,
  birthday: iconWidth * 2,
  address: iconWidth * 3,
  phone: iconWidth * 4,
  password: iconWidth * 5,
};

export const iconOrder: IconType[] = [
  'name',
  'email',
  'birthday',
  'address',
  'phone',
  'password',
];
