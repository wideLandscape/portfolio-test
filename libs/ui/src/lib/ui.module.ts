import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PortfolioCardComponent } from './portfolio-card/portfolio-card.component';
import { IconComponent } from './icon/icon.component';
import { PortfolioCardLabelPipe } from './_pipes/portfolio-card-label.pipe';
import { RandomUserPropertyPipe } from './_pipes/random-user-property.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [
    PortfolioCardComponent,
    IconComponent,
    PortfolioCardLabelPipe,
    RandomUserPropertyPipe,
  ],
  exports: [PortfolioCardComponent],
})
export class UiModule {}
