import { Pipe, PipeTransform } from '@angular/core';
import { IconType, RandomUser } from '@portfolio-test/data-models';

@Pipe({
  name: 'randomUserProperty',
})
export class RandomUserPropertyPipe implements PipeTransform {
  private randomUserParser: Record<IconType, (user: RandomUser) => string> = {
    name: ({ name }) => `${name?.first} ${name?.last}`,
    email: ({ email }) => email,
    birthday: ({ dob }) => new Date(dob.date).toDateString(),
    address: ({ location }) =>
      `${location?.street?.number} ${location?.street?.name}`,
    phone: ({ phone, cell }) => (phone ? phone : cell),
    password: ({ login }) => login.password,
  };

  transform(user: RandomUser, type: IconType): string {
    return this.randomUserParser[type](user);
  }
}
