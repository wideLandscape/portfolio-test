import { Pipe, PipeTransform } from '@angular/core';
import { IconType } from '@portfolio-test/data-models';

@Pipe({
  name: 'portfolioCardLabel',
})
export class PortfolioCardLabelPipe implements PipeTransform {
  private labelMapper: Record<IconType, string> = {
    name: 'Hi, My name is',
    email: 'My email address is',
    birthday: 'My birthday is',
    address: 'My address is',
    phone: 'My phone number is',
    password: 'My password is',
  };
  transform(icon: IconType): string {
    return this.labelMapper[icon];
  }
}
