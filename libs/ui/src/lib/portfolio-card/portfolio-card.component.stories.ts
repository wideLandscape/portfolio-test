import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { IconComponent } from '../icon/icon.component';
import { PortfolioCardLabelPipe } from '../_pipes/portfolio-card-label.pipe';
import { RandomUserPropertyPipe } from '../_pipes/random-user-property.pipe';
import { PortfolioCardComponent } from './portfolio-card.component';

export default {
  title: 'PortfolioCardComponent',
  component: PortfolioCardComponent,
  decorators: [
    moduleMetadata({
      imports: [],
      declarations: [
        IconComponent,
        PortfolioCardLabelPipe,
        RandomUserPropertyPipe,
      ],
    }),
  ],
} as Meta<PortfolioCardComponent>;

const Template: Story<PortfolioCardComponent> = (
  args: PortfolioCardComponent
) => ({
  props: args,
});

const randomUserMockUp = {
  name: { title: 'Monsieur', first: 'Jacques', last: 'Vidal' },
  location: {
    street: { number: 6000, name: "Rue de L'Abbé-De-L'Épée" },
  },
  email: 'jacques.vidal@example.com',
  login: {
    username: 'redsnake528',
    password: 'toby',
  },
  dob: { date: '1966-05-01T02:08:14.446Z', age: 55 },
  phone: '079 705 64 61',
  cell: '077 198 82 88',
  picture: {
    large: 'https://randomuser.me/api/portraits/men/4.jpg',
    medium: 'https://randomuser.me/api/portraits/med/men/4.jpg',
    thumbnail: 'https://randomuser.me/api/portraits/thumb/men/4.jpg',
  },
};

export const Primary = Template.bind({});
Primary.args = { user: randomUserMockUp };
