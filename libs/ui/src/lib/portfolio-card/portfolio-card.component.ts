import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { RandomUser, IconType } from '@portfolio-test/data-models';
import { iconOrder } from '../_configs';

@Component({
  selector: 'portfolio-test-portfolio-card',
  templateUrl: './portfolio-card.component.html',
  styleUrls: ['./portfolio-card.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PortfolioCardComponent {
  @Input()
  user!: RandomUser | null;

  icons: IconType[] = iconOrder;

  selectedIcon: IconType = this.icons[0];

  onMouseOverPortfolioTestIcon(icon: IconType) {
    this.selectedIcon = icon;
  }
}
