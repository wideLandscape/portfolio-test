import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { iconTypesXAxisMap } from '../_configs';
import { IconType, IconTypeXAxisMap } from '@portfolio-test/data-models';
@Component({
  selector: 'portfolio-test-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  @Input()
  type: IconType = 'name';

  @Input()
  hover = false;

  readonly iconTypesConfig: IconTypeXAxisMap = iconTypesXAxisMap;
}
