import { Meta, moduleMetadata, Story } from '@storybook/angular';
import { iconOrder } from '../_configs';
import { IconComponent } from './icon.component';
export default {
  title: 'IconComponent',
  component: IconComponent,
  decorators: [
    moduleMetadata({
      imports: [],
    }),
  ],
  argTypes: {
    type: {
      options: iconOrder,
      control: { type: 'select' },
    },
    hover: { control: { type: 'boolean' }, defaultValue: false },
  },
} as Meta<IconComponent>;

const Template: Story<IconComponent> = (args: IconComponent) => ({
  props: args,
});

export const Name = Template.bind({});
Name.args = { type: 'name' };
export const Selected = Template.bind({});
Selected.args = { type: 'name', hover: true };
export const Email = Template.bind({});
Email.args = { type: 'email' };
export const Birthday = Template.bind({});
Birthday.args = { type: 'birthday' };
export const Address = Template.bind({});
Address.args = { type: 'address' };
export const Phone = Template.bind({});
Phone.args = { type: 'phone' };
export const Password = Template.bind({});
Password.args = { type: 'password' };
