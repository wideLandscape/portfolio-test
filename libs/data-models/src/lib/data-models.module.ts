import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
export * from './random-user.interfaces';
export * from './icon.interfaces';

@NgModule({
  imports: [CommonModule],
})
export class DataModelsModule {}
