export interface RandomUserApiResult {
  results: RandomUser[];
  info?: ApiResultInfo;
}

export interface ApiResultInfo {
  page: number;
  results: number;
  seed: string;
  version: string;
}

export interface RandomUser {
  name: RandomUserName;
  location: RandomUserLocation;
  email: string;
  dob: { date: string; age: number };
  phone: string;
  cell: string;
  picture: RandomUserPicture;
  login: RandomUserLogin;
}
export interface RandomUserName {
  title: string;
  first: string;
  last: string;
}
export interface RandomUserLocation {
  street: { number: number; name: string };
}
export interface RandomUserPicture {
  large: string;
  medium: string;
  thumbnail: string;
}
export interface RandomUserLogin {
  username: string;
  password: string;
}
