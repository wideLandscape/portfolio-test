export type IconType =
  | 'name'
  | 'email'
  | 'birthday'
  | 'address'
  | 'phone'
  | 'password';

export type IconTypeXAxisMap = Record<IconType, number>;
