# Over-engineered Portfolio Card Component

This project was generated using [Nx](https://nx.dev).


Run Web App:

`ng serve portfolio-test`

Run Storybook (shows ui lib isolated from the app)

`ng run ui:storybook`

Cypress E2E test Web App:

`ng e2e portfolio-test-e2e`

Cypress E2E test ui lib:

`ng e2e ui-e2e`

Unit tests:

`ng test api`

`ng test ui`

`ng test core-portfolio-test`
