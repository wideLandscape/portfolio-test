import { getCardComponent } from '../support/app.po';

describe('portfolio-test', () => {
  beforeEach(() => cy.visit('/'));

  it('should display the component creation message', () => {
    // Custom command example, see `../support/commands.ts` file
    cy.login('my-email@something.com', 'myPassword');

    // Function helper example, see `../support/app.po.ts` file
    getCardComponent().get('.icon-label').contains('Hi, My name is');
  });
});
