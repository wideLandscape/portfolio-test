import { TestBed } from '@angular/core/testing';
import { CorePortfolioTestModule } from '@portfolio-test/core/portfolio-test';
import { UiModule } from '@portfolio-test/ui';
import { AppComponent } from './app.component';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiModule, CorePortfolioTestModule],
      declarations: [AppComponent],
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render portfolio-card component', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement as HTMLElement;
    expect(
      compiled.querySelector('portfolio-test-portfolio-card')
    ).toBeTruthy();
  });
});
