import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RandomUser } from '@portfolio-test/data-models';
import { SandboxService } from '@portfolio-test/core/portfolio-test';
import { Observable } from 'rxjs';

@Component({
  selector: 'portfolio-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
  randomUser$: Observable<RandomUser> = this.service.randomUser$;
  error$: Observable<Error> = this.service.error$;

  constructor(private service: SandboxService) {}
}
