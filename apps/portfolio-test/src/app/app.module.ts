import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CorePortfolioTestModule } from '@portfolio-test/core/portfolio-test';
import { UiModule } from '@portfolio-test/ui';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, UiModule, CorePortfolioTestModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
