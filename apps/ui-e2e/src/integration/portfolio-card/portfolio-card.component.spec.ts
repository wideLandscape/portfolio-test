describe('ui', () => {
  beforeEach(() => cy.visit('/iframe.html?id=portfoliocardcomponent--primary'));
  it('should render the component', () => {
    cy.get('portfolio-test-portfolio-card').should('exist');
  });

  it('should add class hover on mouseover event', () => {
    cy.get('portfolio-test-icon:first')
      .trigger('mouseover')
      .get('.icon:first')
      .should('have.class', 'hover');
  });
  it('should not remove class hover on mouseout event', () => {
    cy.get('portfolio-test-icon:first')
      .trigger('mouseover')
      .trigger('mouseout')
      .get('.icon:first')
      .should('have.class', 'hover');
  });
});
