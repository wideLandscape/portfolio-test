describe('ui', () => {
  beforeEach(() => cy.visit('/iframe.html?id=iconcomponent--name'));
  it('should render the component', () => {
    cy.get('portfolio-test-icon').should('exist');
  });
});
